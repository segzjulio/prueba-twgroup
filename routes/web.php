<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\LogsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [HomeController::class, 'index'])->middleware(['auth'])->name('dashboard');
Route::get('/create-task', [TasksController::class, 'create'])->middleware(['auth'])->name('create-task');
Route::post('/store-task', [TasksController::class, 'store'])->middleware(['auth'])->name('store-task');
Route::post('/update-task/{id}', [TasksController::class, 'update'])->middleware(['auth'])->name('update-task');
Route::get('/delete-task/{id}', [TasksController::class, 'destroy'])->middleware(['auth'])->name('delete-task');

Route::get('/logs/{id}', [LogsController::class, 'index'])->middleware(['auth'])->name('logs');
Route::post('/store-logs', [LogsController::class, 'store'])->middleware(['auth'])->name('store-logs');
Route::post('/update-log/{id}/{id_task}', [LogsController::class, 'update'])->middleware(['auth'])->name('update-logs');
Route::get('/delete-log/{id}/{id_task}', [LogsController::class, 'destroy'])->middleware(['auth'])->name('delete-log');

require __DIR__.'/auth.php';
