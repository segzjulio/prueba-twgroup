<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;

class LogsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // dd($id);
        $logs = Log::with('task')->where('task_id', $id)->get();
        $taskId = $id;
        return view('logs', compact('logs', 'taskId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'comentarios' => ['required'],
            'id_task' => ['required']
        ]);
        $log = new Log();
        $log->comentarios = $request->comentarios;
        $log->task_id = $request->id_task;

        $log->save();

        return redirect("/logs/$request->id_task");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $id_task)
    {
        // dd($request, $id, $id_task);
        $log = Log::where('id', $id)->first();
        $log->comentarios = $request->comentarios;
        $log->save();

        return redirect("/logs/$id_task");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $id_task)
    {
        $log = Log::where('id', $id)->first();
        $log->delete();
        return redirect("/logs/$id_task");
    }
}
