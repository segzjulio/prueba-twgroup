<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;


class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('user')->get();
        $users = User::all();
        return view('dashboard', compact('tasks', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();

        return view('create-task', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd("hola", $request->all());
        // dd($request);
        $request->validate([
            'tarea' => ['required', 'string', 'max:255'],
            'id_user' => ['required', 'integer', 'exists:App\Models\User,id'],
            'fecha_maxima' => ['required', 'date']
        ]);

        $task = new Task();
        $task->user_id = $request->id_user;
        $task->descripcion = $request->tarea;
        $task->fecha_maxima = $request->fecha_maxima;

        $task->save();

        return redirect('/dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request, $id);
        $task = Task::where('id', $id)->first();
        $task->user_id = $request->id_user;
        $task->descripcion = $request->tarea;
        $task->fecha_maxima = $request->fecha_maxima;
        $task->save();

        return redirect('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::where('id', $id)->first();
        $task->delete();
        return redirect('/dashboard');
    }
}
