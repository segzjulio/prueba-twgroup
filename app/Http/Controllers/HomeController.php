<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;

class HomeController extends Controller
{
    public function index(){
        $tasks = Task::with('user')->get();
        $users = User::all();
        return view('dashboard', compact('tasks', 'users'));
    }
}
