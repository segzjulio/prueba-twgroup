<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotHackedMail implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $mail = explode('@',$value);

        if(count($mail)<2){
            return false;
        }

        return strtolower($mail[1]) != 'hack.net';
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El host del mail no puede ser hack.net';
    }
}
