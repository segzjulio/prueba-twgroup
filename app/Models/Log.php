<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;

class Log extends Model
{
    protected $fillable = [
        'task_id',
        'comentarios',
    ];

    public function task(){
        return $this->belongsTo(Task::class, 'task_id');
    }
}
