<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Task extends Model
{
    protected $fillable = [
        'user_id',
        'descripcion',
        'fecha_maxima',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
