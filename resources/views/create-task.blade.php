<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Tarea</h5>
                    <form action="{{ route('store-task') }}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label for="tarea">Tarea: </label>
                                <input type="text" class="form-control" id="tarea" name="tarea">
                            </div>
                            <div class="col-sm-12">
                                <label for="perfil">Usuario:</label>
                                <select class="form-control" id="id_user" name="id_user" data-placeholder="Seleccione opcion(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                    <option disable>Seleccionar Usuario...</option>
                                    @foreach ($users as $us)
                                        <option value={{$us->id}} >{{$us->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <label for="">Fecha Maxima: </label>
                                <input type="datetime-local" class="form-control" id="fecha_maxima" name="fecha_maxima">
                            </div>
                        </div>
                        <div>
                            <div class="modal-footer">
                                <a type="button" class="btn btn-danger" href="/dashboard" data-dismiss="modal">Cancelar</a>
                                <button type="submit" class="btn btn-success" style="color:white;">Crear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
