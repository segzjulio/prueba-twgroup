<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <span></span>
                                </div>
                                <div class="col-sm-2">
                                    <a class="btn btn-success" type="button" href="/create-task" title="Agregar Tarea">Agregar Tarea</a>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="tableUsers">
                                <thead style="font-size:12px">
                                    <th>ID</th>
                                    <th>Descripcion Tarea</th>
                                    <th>Usuario</th>
                                    <th>Fecha Maxima</th>
                                    <th><i class="fa fa-cog"></i></th>
                                </thead>
                                <tbody>
                                    @foreach ($tasks as $ta)
                                        <tr style="font-size:12px">
                                            <td>{{$ta->id}}</td>
                                            <td>{{$ta->descripcion}}</td> 
                                            <td>{{$ta->user->name}}</td> 
                                            <td>{{$ta->fecha_maxima}}</td> 
                                            <td>
                                                <div class="btn-group">
                                                    @if(Auth::id() == $ta->user_id)
                                                        <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#bd-example-modal-lg" data-whatever="{{ $ta }}">Editar</button>
                                                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal" data-whatever="{{ $ta }}"">Borrar</button>                                                
                                                        <a type="button" class="btn btn-primary btn-xs" href="/logs/{{$ta->id}}">Agregar Logs</a>                                                
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header" style="background-color: #0489B1; color:white">
                                        <h5 class="modal-title" id="exampleModalLabel">Crear perfil</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="#" id="editar" method="POST">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="col-sm-12">
                                                <label for="">Tarea: </label>
                                                <input type="text" class="form-control" id="tarea" name="tarea" placeholder="Nombre de bodega">
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="perfil">Usuario:</label>
                                                <select class="form-control" id="id_user" name="id_user" data-placeholder="Seleccione pabellon(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                                    <option value="">Seleccionar usuario...</option>
                                                    @foreach ($users as $us)
                                                        <option value={{$us->id}} >{{$us->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-6" style="margin-top:10px;">
                                                <label for="">Fecha maxima: </label>
                                                <input type="datetime-local" id="fecha_maxima" name="fecha_maxima" class="form-control">
                                            </div>
                                        </div>
                                        <div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-warning" style="color:white;">Editar</button>
                                                {{-- <a href="#" id="eliminar-user" type="button" class="btn btn-warning">Editar</a> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="background-color: #0489B1; color:white">
                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar tarea</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:white">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                        <a href="#" id="eliminar-tarea" type="button" class="btn btn-warning">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .inline {
            display: inline-flex
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/dayjs@1.10.7/dayjs.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#bd-example-modal-lg').on('show.bs.modal', function (event) {
                // console.log(event);
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                var modal = $(this)
                modal.find("#tarea").attr("value", recipient.descripcion);
                modal.find("#fecha_maxima").val(dayjs(recipient.fecha_maxima).format('YYYY-MM-DDTHH:mm'));
                modal.find("#id_user").val(recipient.user_id).trigger('change');

                modal.find("#editar").attr("action", `{{ url('/update-task/${recipient.id}') }}`);
            });
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                var modal = $(this)
                modal.find('.modal-body').text('Esta seguro que desea eliminar la tarea "'+recipient.descripcion+'"')
                modal.find("#eliminar-tarea").attr("href", `/delete-task/${recipient.id}`);
            })
        } );
    </script>
</x-app-layout>

